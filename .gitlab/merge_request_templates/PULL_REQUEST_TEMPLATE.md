## 拉取/合并请求描述：

[
这段方括号里的内容是您**必须填写并替换掉**的，否则PR不可能被合并。**方括号外面的内容不需要修改，但请仔细阅读。**
请在这里填写您的PR描述，可以包括以下之一的内容：
为什么提交这份PR；解决的问题是什么，你的解决方案是什么；
并确认并列出已经在什么情况或板卡上进行了测试；
并且附上测试结果于测试流程，如果代码存在耦合，则需要对耦合部分进行修改。

]


以下的内容不应该在提交PR时的message修改，修改下述message，PR会被直接关闭。请在提交PR后，浏览器查看PR并对以下检查项逐项check，没问题后逐条在页面上打钩。

### 当前拉取/合并请求的状态

必须选择一项 :

- [ ] 本拉取/合并请求是一个草稿版本 
- [ ] 本拉取/合并请求是一个成熟版本 

### 代码质量 Code Quality：

我在这个拉取/合并请求中已经考虑了 As part of this pull request, I've considered the following:

- [ ] 已经仔细查看过代码改动的对比 Already check the difference between PR and old code
- [ ] 代码风格正确，包括缩进空格，命名及其他风格 Style guide is adhered to, including spacing, naming and other styles
- [ ] 没有垃圾代码，代码尽量精简，不包含`#if 0`代码，不包含已经被注释了的代码 All redundant code is removed and cleaned up
- [ ] 所有变更均有原因及合理的，并且不会影响到其他软件组件代码或BSP All modifications are justified and not affect other components or BSP
- [ ] 对难懂代码均提供对应的注释 I've commented appropriately where code is tricky
- [ ] 本拉取/合并请求代码是高质量的 Code in this PR is of high quality
- [ ] 本拉取/合并符合[代码规范](../documentation/coding_style_cn.md) 
